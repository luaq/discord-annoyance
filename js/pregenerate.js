
let message = process.env.PREGENERATE_MESSAGE;
const splitBy = process.env.PREGENERATE_SPLIT;

let split = message.split(splitBy);

for (var i = 0; i < split.length; i++) {
  split[i] = "||" + split[i] + "||";
}

message = split.join(splitBy);
console.log(message);
