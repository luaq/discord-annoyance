
/*
 * Copyright (c) 2019 Luaq.CC All Rights Reserved.
 * I am not responsible if you get banned for account botting.
 */

/* Settings */

const allowedChannels = ["dm", "group"];
const splitBy = " "; // this will replace every word with spoilers

/* End of Settings */

var isEnabled = false;

const Discord = require("discord.js");
const client = new Discord.Client();

client.on("ready", () => {
  console.log(`Logged in with token as ${client.user.tag}`);

  isEnabled = true;
});

client.on("message", (msg) => {
  if (msg.author.id !== client.user.id) return;
  let message = msg.content;

  if (msg.channel.type === "group" && msg.channel.name === client.user.id) {
    if (message === "toggle") {
      isEnabled = !isEnabled;
      let s = (isEnabled) ? "on" : "off";
      msg.channel.send(new Discord.RichEmbed().setTitle("Toggled " + s).setDescription("Turned " + s + " the annoyance features."));
    }
    return;
  }

  if (allowedChannels.indexOf(msg.channel.type) !== -1 && isEnabled) {
    let splitArr = message.split(splitBy);
    console.log("Found message sent with ID: " + msg.id);
    console.log(`[${msg.id}] Formatting ${splitArr.length} strings...`);

    for (var i = 0; i < splitArr.length; i++) {
      splitArr[i] = "||" + splitArr[i] + "||";
    }

    console.log(`[${msg.id}] Editing...`);
    message = splitArr.join(splitBy);
    msg.edit(message).catch(err => console.log(`[${msg.id}] Message has surpassed 2000 characters.`));

    console.log(`[${msg.id}] Finished!`);
  }

});

console.clear();
console.log("Logging in...");

const CLIENT_TOKEN = process.env.CLIENT_TOKEN;

client.login(CLIENT_TOKEN).catch(err => {
  let token = CLIENT_TOKEN.split("\"");
  if (token.length <= 1) {
    console.log("The token that was provided is invalid or you have no internet connection.");
  } else {
    console.log("You did not remove the quotes from your token.");
    console.log("Attempting without quotes...");
    let clientToken = token.join("");
    console.log("Logging in with new token...");
    client.login(clientToken).catch(e => console.log("\nFailed, aborting..."));
  }
});
